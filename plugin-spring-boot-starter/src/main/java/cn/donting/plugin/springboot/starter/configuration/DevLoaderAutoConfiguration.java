package cn.donting.plugin.springboot.starter.configuration;

import cn.donting.plugin.springboot.starter.application.PluginApplication;
import cn.donting.plugin.springboot.starter.condition.ConditionOnPluginEnv;
import cn.donting.plugin.springboot.starter.env.PluginDevProperty;
import cn.donting.plugin.springboot.starter.exception.PluginException;
import cn.donting.plugin.springboot.starter.extension.AbsPluginManager;
import cn.donting.plugin.springboot.starter.loader.PluginDevLoader;
import cn.donting.plugin.springboot.starter.loader.PluginURLClassLoader;
import cn.donting.plugin.springboot.starter.loader.dev.BootDevPluginLoader;
import cn.donting.plugin.springboot.starter.loader.dev.BootWebDevPluginLoader;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.OrderComparator;
import org.springframework.core.env.Environment;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Map;

@Configuration
@ConditionOnPluginEnv(dev=true)
@EnableConfigurationProperties(PluginDevProperty.class)
@Slf4j
public class DevLoaderAutoConfiguration implements ApplicationRunner {

    @Autowired
    PluginDevProperty pluginDevProperty;

    @Autowired
    ApplicationContext applicationContext;

    @Autowired
    AbsPluginManager pluginManager;

    @Bean
    public BootDevPluginLoader bootDevPluginLoader() {
        return new BootDevPluginLoader();
    }

    @Bean
    public BootWebDevPluginLoader bootWebDevPluginLoader() {
        return new BootWebDevPluginLoader();
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {

        Environment environment = applicationContext.getEnvironment();
        if(pluginDevProperty.getPluginNum()==0){
            return;
        }
        for (int i = 0; i <pluginDevProperty.getPluginNum() ; i++) {
            String classpath = environment.getProperty(PluginDevProperty.PREFIX_CLASSPATH + i);
            String mainClass = environment.getProperty(PluginDevProperty.PREFIX_MAIN_CLASS + i);
            DevRunClass devRunClass=new DevRunClass(classpath,mainClass);
            try {
                PluginURLClassLoader pluginURLClassLoader = devRunClass.getPluginURLClassLoader();
                PluginDevLoader devLoader = getDevLoader(pluginURLClassLoader);
                PluginApplication pluginApplication = devLoader.loader(pluginURLClassLoader, mainClass);
                pluginManager.loadDevPlugin(pluginApplication);
            }catch (PluginException ex){
                log.error(mainClass+ex.getMessage());
                throw ex;
            }
        }
    }

    /**
     * 获取dev加载器
     * @param pluginURLClassLoader
     * @return
     * @throws PluginException
     */
    public PluginDevLoader getDevLoader(PluginURLClassLoader pluginURLClassLoader) throws PluginException {
        Map<String, PluginDevLoader> beansOfType = applicationContext.getBeansOfType(PluginDevLoader.class);
        for (Map.Entry<String, PluginDevLoader> entry : beansOfType.entrySet()) {
            PluginDevLoader value = entry.getValue();
            if (value.isLoader(pluginURLClassLoader)) {
                return value;
            }
        }
        throw new PluginException("未找到合适的加载器");
    }

    @Getter
    private static class DevRunClass{
        ArrayList<String> classpaths=new ArrayList<>();
        String mainClass;
        public DevRunClass(String classpath, String mainClass) {
            String[] classpathss = classpath.split(";");
            for (String cp : classpathss) {
                classpaths.add(cp);
            }
            this.mainClass = mainClass;
        }

        PluginURLClassLoader getPluginURLClassLoader() throws MalformedURLException {
            ArrayList<URL> urls=new ArrayList<>();
            for (String classpath : classpaths) {
                urls.add(new File(classpath).toURI().toURL());
            }
            URL[] urlss = urls.toArray(new URL[0]);
            PluginURLClassLoader pluginURLClassLoader=new PluginURLClassLoader(urlss,DevLoaderAutoConfiguration.class.getClassLoader().getParent());
            return pluginURLClassLoader;
        }
    }
}
