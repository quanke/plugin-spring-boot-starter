package cn.donting.plugin.springboot.starter.extension.lmp;

import cn.donting.plugin.springboot.starter.extension.IPluginDispatcher;

import javax.servlet.http.HttpServletRequest;

/**
 * 默认的  IPluginDispatcher
 * 取 query 中的 pluginId
 */
public class DefaultPluginDispatcher implements IPluginDispatcher {
    @Override
    public String getPluginId(HttpServletRequest httpServletRequest) {
        String pluginId = httpServletRequest.getParameter("pluginId");
        return pluginId;
    }
}
