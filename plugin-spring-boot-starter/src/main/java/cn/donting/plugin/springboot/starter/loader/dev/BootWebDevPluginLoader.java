package cn.donting.plugin.springboot.starter.loader.dev;

import cn.donting.plugin.springboot.starter.application.BootApplication;
import cn.donting.plugin.springboot.starter.application.BootWebApplication;
import cn.donting.plugin.springboot.starter.application.PluginApplication;
import cn.donting.plugin.springboot.starter.exception.PluginException;
import cn.donting.plugin.springboot.starter.loader.PluginDevLoader;
import cn.donting.plugin.springboot.starter.loader.PluginURLClassLoader;
import cn.donting.plugin.springboot.starter.servlet.PluginServletContext;
import cn.donting.springboot.plugin.top.domain.PluginInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.annotation.AnnotationAwareOrderComparator;

import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;

/**
 * springboot jar 加载器 (非web)
 *
 * @author donting
 */
@Slf4j
public class BootWebDevPluginLoader extends BootDevPluginLoader {


    /**
     * 创建 PluginApplication
     *
     * @param classLoader 类加载器
     * @param mainClass   主类
     * @param pluginInfo  插件信息
     * @return
     */
    protected PluginApplication creatPluginApplication(PluginURLClassLoader classLoader, Class<?> mainClass, PluginInfo pluginInfo) {
        PluginApplication pluginApplication = new BootWebApplication(classLoader, new PluginServletContext(), mainClass, pluginInfo);
        return pluginApplication;
    }


    @Override
    public boolean isLoader(PluginURLClassLoader classLoader) {
        if (super.isSpringBoot(classLoader)) {
            URL[] urLs = classLoader.getURLs();
            try {
                for (URL urL : urLs) {
                    String path = null;
                    path = urL.toURI().toURL().getPath();
                    if (path.contains("spring-webmvc")) {
                        return true;
                    }
                }
            } catch (Exception e) {
                log.debug(e.getMessage(), e);
            }
        }
        return false;
    }

}
