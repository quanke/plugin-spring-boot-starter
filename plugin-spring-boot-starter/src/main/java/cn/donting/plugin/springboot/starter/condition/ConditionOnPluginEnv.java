package cn.donting.plugin.springboot.starter.condition;

import org.springframework.context.annotation.Conditional;

import java.lang.annotation.*;

/**
 * 是否是 处于dev模式
 * 用于区分主程序和插件
 */
@Target({ ElementType.TYPE, ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Conditional(OnPluginCondition.class)
public @interface ConditionOnPluginEnv {
    boolean dev();
}
