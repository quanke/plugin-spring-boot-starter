package cn.donting.plugin.springboot.starter.loader;

import cn.donting.plugin.springboot.starter.extension.AbsPluginManager;
import cn.donting.springboot.plugin.top.domain.PluginInfo;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;

/**
 * 插件的类加载器
 */
public class PluginURLClassLoader extends URLClassLoader {
    private PluginInfo pluginInfo;
    private String name;

    public PluginURLClassLoader(URL[] urls, ClassLoader parent) {
        super(urls, parent);
//        URL resource = PluginURLClassLoader.class.getClassLoader().getResource(PluginURLClassLoader.class.getName() + ".class");
        File file = new File(AbsPluginManager.PLUGINS_LIB_PATH);
        for (File listFile : file.listFiles()) {
            try {
                addURL(listFile.toURI().toURL());
            } catch (MalformedURLException e) {
               throw new RuntimeException(e);
            }
        }
    }

    public void setPluginInfo(PluginInfo pluginInfo) {
        this.pluginInfo = pluginInfo;
        this.name = pluginInfo.getName();
    }

    public PluginInfo getPluginInfo() {
        return pluginInfo;
    }
}
