package cn.donting.plugin.springboot.starter.extension.lmp;

import cn.donting.plugin.springboot.starter.extension.PluginInfoDb;
import cn.donting.plugin.springboot.starter.entity.PluginInstallInfo;
import cn.donting.plugin.springboot.starter.exception.PluginRuntimeException;
import cn.donting.springboot.plugin.top.domain.PluginInfo;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@Slf4j
public class JsonPluginInfoDb implements PluginInfoDb {
    private String filePath = PluginManager.PLUGINS_PATH + File.separator + "pluginInfoDb.json";
    private final ObjectMapper objectMapper = new ObjectMapper();

    public JsonPluginInfoDb() {
        File file = new File(filePath);
        if (!file.exists()) {
            try {
                DbInfo dbInfo=new DbInfo();
                saveDbInfo(dbInfo);
                file.createNewFile();
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
        }
    }

    @Override
    public synchronized PluginInstallInfo save(PluginInstallInfo pluginInfo) {
        try {
            DbInfo dbInfo = getDbInfo();
            HashMap<String, PluginInstallInfo> plugins = dbInfo.plugins;
            if (plugins.containsKey(pluginInfo.getId())) {
                throw new PluginRuntimeException(pluginInfo.getId() + " 已经安装");
            }
            plugins.put(pluginInfo.getId(), pluginInfo);
            saveDbInfo(dbInfo);
            return  pluginInfo;
        } catch (Exception e) {
            throw new PluginRuntimeException(e);
        }
    }

    @Override
    public synchronized PluginInstallInfo delete(String pluginId) {
        try {
            DbInfo dbInfo = getDbInfo();
            HashMap<String, PluginInstallInfo> plugins = dbInfo.plugins;
            PluginInstallInfo remove = plugins.remove(pluginId);
            saveDbInfo(dbInfo);
            return remove;
        } catch (Exception e) {
            throw new PluginRuntimeException(e);
        }
    }

    @Override
    public synchronized PluginInstallInfo get(String pluginId) {
        try {
            DbInfo dbInfo = getDbInfo();
            HashMap<String, PluginInstallInfo> plugins = dbInfo.plugins;
            return plugins.get(pluginId);
        } catch (Exception e) {
            throw new PluginRuntimeException(e);
        }
    }

    @Override
    public synchronized PluginInstallInfo update(PluginInstallInfo pluginInfo) {
        try {
            DbInfo dbInfo = getDbInfo();
            HashMap<String, PluginInstallInfo> plugins = dbInfo.plugins;
            PluginInfo remove = plugins.remove(pluginInfo.getId());
            plugins.put(pluginInfo.getId(),pluginInfo);
            saveDbInfo(dbInfo);
            return pluginInfo;
        } catch (Exception e) {
            throw new PluginRuntimeException(e);
        }
    }

    @Override
    public synchronized ArrayList<PluginInstallInfo> getAll() {
        try {
            DbInfo dbInfo = getDbInfo();
            HashMap<String, PluginInstallInfo> plugins = dbInfo.getPlugins();
            ArrayList<PluginInstallInfo> pluginInfos=new ArrayList<>();
            for (Map.Entry<String, PluginInstallInfo> stringPluginInstallInfoEntry : plugins.entrySet()) {
                PluginInstallInfo pluginInstallInfo = stringPluginInstallInfoEntry.getValue();
                pluginInfos.add(pluginInstallInfo);
            }
            return pluginInfos;
        } catch (Exception e) {
            throw new PluginRuntimeException(e);
        }
    }


    private final DbInfo getDbInfo() throws Exception {
        File dbFile = new File(filePath);
        DbInfo dbInfo = objectMapper.readValue(dbFile, DbInfo.class);
        return dbInfo;
    }

    private final void saveDbInfo(DbInfo dbInfo) throws Exception {
        File dbFile = new File(filePath);
        objectMapper.writeValue(dbFile, dbInfo);
    }


    @Data
    private static class DbInfo {
        public final int version = 100;
        private HashMap<String, PluginInstallInfo> plugins = new HashMap();
    }
}
