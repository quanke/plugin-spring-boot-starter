package cn.donting.plugin.springboot.starter.exception;

public class PluginNotSupportException extends PluginRuntimeException {
    public PluginNotSupportException() {
        super("未支持!!!");
    }

    public PluginNotSupportException(String message) {
        super(message);
    }

    public PluginNotSupportException(String message, Throwable cause) {
        super("未支持!!!", cause);
    }
}
