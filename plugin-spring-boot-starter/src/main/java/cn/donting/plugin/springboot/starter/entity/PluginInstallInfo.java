package cn.donting.plugin.springboot.starter.entity;

import cn.donting.springboot.plugin.top.domain.PluginInfo;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.File;

/**
 * 插件安装信息
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class PluginInstallInfo extends PluginInfo {
    String installFilePath;
    Long installTime;

    public static PluginInstallInfo build(File file, PluginInfo pluginInfo) {
        PluginInstallInfo pluginInstallInfo = new PluginInstallInfo();
        pluginInstallInfo.setInstallFilePath(file.getPath());
        pluginInstallInfo.setInstallTime(System.currentTimeMillis());
        pluginInstallInfo.setId(pluginInfo.getId());
        pluginInstallInfo.setName(pluginInfo.getName());
        pluginInstallInfo.setVersion(pluginInfo.getVersion());
        pluginInstallInfo.setNumberVersion(pluginInfo.getNumberVersion());
        return pluginInstallInfo;
    }
}
