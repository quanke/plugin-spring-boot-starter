package cn.donting.plugin.springboot.starter.extension;

import cn.donting.plugin.springboot.starter.exception.PluginException;
import cn.donting.plugin.springboot.starter.loader.PluginURLClassLoader;
import cn.donting.springboot.plugin.top.domain.PluginInfo;

/**
 *从  PluginURLClassLoader 中去加载插件信息
 */
public interface IPluginInfoLoader {

    /**
     * 获取 PluginInfo
     * @param mainClass 启动类
     * @param pluginURLClassLoader 类加载器
     * @return  PluginInfo 不能为空
     * @throws  PluginException 加载不了时 抛出此异常
     */
    PluginInfo get(Class<?> mainClass, PluginURLClassLoader pluginURLClassLoader) throws PluginException;
}
