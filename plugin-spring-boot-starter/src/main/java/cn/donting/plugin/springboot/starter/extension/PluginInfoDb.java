package cn.donting.plugin.springboot.starter.extension;

import cn.donting.plugin.springboot.starter.entity.PluginInstallInfo;

import java.util.ArrayList;

/**
 * 用于保存  插件安装信息的 db 类
 */
public interface PluginInfoDb {
    /**
     * 保存插件安装信息
     * @param PluginInstallInfo
     * @return PluginInstallInfo
     */
    PluginInstallInfo save(PluginInstallInfo pluginInstallInfo);

    /**
     * 删除插件安装信息
     * @param pluginId 插件Id
     * @return
     */
    PluginInstallInfo delete(String pluginId);

    /**
     * 获取插件安装信息
     * @param pluginId 插件Id
     * @return
     */
    PluginInstallInfo get(String pluginId);

    /**
     * 更新插件安装信息
     * @param PluginInstallInfo PluginInstallInfo
     * @return
     */
    PluginInstallInfo update(PluginInstallInfo pluginInstallInfo);

    /**
     * 获取所有的 插件安装信息
     * @return
     */
    ArrayList<PluginInstallInfo> getAll();
}
