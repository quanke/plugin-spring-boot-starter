package cn.donting.plugin.springboot.starter.exception;

/**
 * @author donting
 */
public class PluginRuntimeException extends RuntimeException{
    public PluginRuntimeException() {
    }

    public PluginRuntimeException(String message) {
        super(message);
    }

    public PluginRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }

    public PluginRuntimeException(Throwable cause) {
        super(cause);
    }

    public PluginRuntimeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
