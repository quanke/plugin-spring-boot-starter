package cn.donting.plugin.springboot.starter.env;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Data
@ConfigurationProperties(prefix = "cn.donting.plugin.dev")
public class PluginDevProperty {
    /**
     * dve插件 classpath ;分割
     * dve插件 mainClass
     * 多个 cn.donting.plugin.dev.classpath1=‘’ cn.donting.plugin.dev.mainClass1=‘’
     *     cn.donting.plugin.dev.classpath2=‘’ cn.donting.plugin.dev.mainClass2=‘’
     */
    public static final String PREFIX_CLASSPATH="cn.donting.plugin.dev.classpath";
    public static final String PREFIX_DEV="cn.donting.plugin.dev";
    public static final String PREFIX_MAIN_CLASS="cn.donting.plugin.dev.mainClass";
    /**
     * 是否开启dev
     */
    private boolean dev=false;
    /**
     * dev 插件数量
     */
    private int pluginNum;


}
