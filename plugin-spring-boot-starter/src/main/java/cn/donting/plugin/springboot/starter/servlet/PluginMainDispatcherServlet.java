package cn.donting.plugin.springboot.starter.servlet;


import cn.donting.plugin.springboot.starter.application.PluginApplication;
import cn.donting.plugin.springboot.starter.extension.lmp.PluginManager;
import cn.donting.plugin.springboot.starter.extension.IPluginDispatcher;
import cn.donting.springboot.plugin.top.web.PluginWebDispatcher;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * 用于分发http请求
 * 该类  中不能注入 httpServlet 相关，此时httpServlet 还未绑定到线程中
 *
 * @author donting
 * @see org.springframework.web.servlet.FrameworkServlet#processRequest
 * 2021-05-30 下午2:41
 */
@Slf4j
public class PluginMainDispatcherServlet extends DispatcherServlet {

    @Autowired
    IPluginDispatcher iPluginDispatcher;

    @Autowired
    PluginManager pluginManager;

    public PluginMainDispatcherServlet() {
        logger.info("PluginMainDispatcherServlet ......");
    }

    @Override
    public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {
        String pluginId = iPluginDispatcher.getPluginId((HttpServletRequest) req);
        if (pluginId == null) {
            super.service(req, res);
            return;
        }
        PluginApplication plugin = pluginManager.getRunPlugin(pluginId);
        if (plugin == null) {
            super.service(req, res);
            return;
        }
        if (plugin instanceof PluginWebDispatcher) {
            ((PluginWebDispatcher) plugin).doService(req,res);
        }

    }


}
