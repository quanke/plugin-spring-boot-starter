package cn.donting.plugin.springboot.starter.extension;

import javax.servlet.http.HttpServletRequest;

/**
 *plugin Dispatcher 分发器
 */
public interface IPluginDispatcher {

    /**
     * 根据 HttpServletRequest 返回插件Id
     * @param httpServletRequest
     * @return pluginId    返回null 则走主程序
     */
    String getPluginId(HttpServletRequest httpServletRequest);
}
