package cn.donting.plugin.springboot.starter.extension.lmp;

import cn.donting.plugin.springboot.starter.application.PluginApplication;
import cn.donting.plugin.springboot.starter.extension.AbsPluginManager;
import cn.donting.plugin.springboot.starter.extension.PluginInfoDb;
import cn.donting.plugin.springboot.starter.entity.PluginInstallInfo;
import cn.donting.plugin.springboot.starter.exception.PluginException;
import cn.donting.plugin.springboot.starter.exception.PluginNotSupportException;
import cn.donting.plugin.springboot.starter.loader.PluginLoader;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;

import java.io.File;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.UUID;

@Slf4j
public class PluginManager extends AbsPluginManager implements ApplicationRunner {

    @Autowired
    PluginInfoDb pluginInfoDb;

    @Override
    public synchronized void install(File file) throws PluginException {
        PluginLoader pluginLoader = getPluginLoader(file);
        File target = new File(PluginManager.PLUGINS_PATH + File.separator + UUID.randomUUID().toString());
        PluginApplication pluginApplication = null;
        try {
            Files.copy(file.toPath(), target.toPath());
            pluginApplication = pluginLoader.loader(target);
            PluginInstallInfo pluginInstallInfo = PluginInstallInfo.build(target, pluginApplication.getPluginInfo());
            log.info("按装插件：id:[{}],name:[{}]",pluginInstallInfo.getId(),pluginInstallInfo.getName());
            pluginInfoDb.save(pluginInstallInfo);
            startPlugin(pluginInstallInfo.getId());
        } catch (PluginException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new PluginException("启动失败", ex);
        }
        super.runPlugins.put(pluginApplication.getPluginInfo().getId(), pluginApplication);

    }

    @Override
    public synchronized void unInstall(String pluginId) throws PluginException{
        try {
            PluginApplication pluginApplication = runPlugins.get(pluginId);
            if (pluginApplication != null) {
                stopPlugin(pluginId);
            }
            PluginInstallInfo delete = pluginInfoDb.delete(pluginId);
            new File(delete.getInstallFilePath()).delete();
        } catch (PluginException ex) {
            throw new PluginException("卸载失败", ex);
        } catch (Exception ex) {
            throw new PluginException("卸载失败", ex);
        }
    }

    @Override
    public synchronized void startPlugin(String pluginId) throws PluginException {
        try {
            PluginInstallInfo pluginInstallInfo = pluginInfoDb.get(pluginId);
            log.info("启动插件：id:[{}],name:[{}]",pluginInstallInfo.getId(),pluginInstallInfo.getName());
            String installFilePath = pluginInstallInfo.getInstallFilePath();
            PluginLoader pluginLoader = getPluginLoader(new File(installFilePath));
            PluginApplication pluginApplication = pluginLoader.loader(new File(installFilePath));
            pluginApplication.run();
            super.runPlugins.put(pluginId, pluginApplication);
        } catch (PluginException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new PluginException("启动失败", ex);
        }
    }

    @Override
    public synchronized int stopPlugin(String pluginId) throws PluginException {
        try {
            PluginApplication remove = runPlugins.remove(pluginId);
            int stop = remove.stop();
            log.info("插件停止exit code[]：[{}],name:[{}]",stop,remove.getPluginInfo().getId(),remove.getPluginInfo().getName());
            return stop;
        } catch (PluginException ex) {
            throw new PluginException("停止 异常", ex);
        } catch (Exception ex) {
            throw new PluginException("停止 异常", ex);
        }
    }

    @Override
    public void updatePlugin(File file) {
        throw new PluginNotSupportException();
    }

    /**
     * 启动自动加载 安装的插件
     * @param args
     * @throws Exception
     */
    @Override
    public void run(ApplicationArguments args) throws Exception {
        ArrayList<PluginInstallInfo> all = pluginInfoDb.getAll();
        for (PluginInstallInfo pluginInstallInfo : all) {
            startPlugin(pluginInstallInfo.getId());
        }
    }
}
