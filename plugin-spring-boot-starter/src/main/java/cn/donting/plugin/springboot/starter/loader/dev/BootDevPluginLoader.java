package cn.donting.plugin.springboot.starter.loader.dev;

import cn.donting.plugin.springboot.starter.application.BootApplication;
import cn.donting.plugin.springboot.starter.application.PluginApplication;
import cn.donting.plugin.springboot.starter.exception.PluginException;
import cn.donting.plugin.springboot.starter.loader.PluginDevLoader;
import cn.donting.plugin.springboot.starter.loader.PluginLoader;
import cn.donting.plugin.springboot.starter.loader.PluginURLClassLoader;
import cn.donting.plugin.springboot.starter.loader.boot.BootLauncher;
import cn.donting.springboot.plugin.top.domain.PluginInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.loader.archive.JarFileArchive;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.jar.JarFile;
import java.util.zip.ZipEntry;

/**
 * springboot jar 加载器 (非web)
 *
 * @author donting
 */
@Slf4j
public class BootDevPluginLoader extends PluginDevLoader {


    /**
     * 创建 PluginApplication
     *
     * @param classLoader 类加载器
     * @param mainClass   主类
     * @param pluginInfo  插件信息
     * @return
     */
    protected PluginApplication creatPluginApplication(PluginURLClassLoader classLoader, Class<?> mainClass, PluginInfo pluginInfo) {
        PluginApplication pluginApplication = new BootApplication(classLoader, mainClass, pluginInfo);
        return pluginApplication;
    }


    @Override
    public boolean isLoader(PluginURLClassLoader classLoader) {
        try {
            URL[] urLs = classLoader.getURLs();
            for (URL urL : urLs) {
                String path = urL.toURI().toURL().getPath();
                if (path.contains("spring-webmvc")) {
                    return false;
                }
            }
            return isSpringBoot(classLoader);
        } catch (Exception ex) {
            log.debug(ex.getMessage(), ex);
        }
        return false;
    }

    protected final boolean isSpringBoot(PluginURLClassLoader classLoader){
        try {
            URL[] urLs = classLoader.getURLs();
            for (URL urL : urLs) {
                String path = urL.toURI().toURL().getPath();
                if (path.contains("spring-boot-starter")) {
                    return true;
                }
            }
            return false;
        } catch (Exception ex) {
            log.debug(ex.getMessage(), ex);
        }
        return false;
    }

    @Override
    public PluginApplication loader(PluginURLClassLoader classLoader,String mainClass) throws PluginException {
        try {
            Class<?> aClass = classLoader.loadClass(mainClass);
            PluginInfo pluginInfo = getPluginInfo(aClass, classLoader);
            PluginApplication pluginApplication = creatPluginApplication(classLoader, aClass, pluginInfo);
            return pluginApplication;
        } catch (ClassNotFoundException e) {
            log.error("未找到主类："+mainClass);
           log.error(e.getMessage(),e);
           throw new PluginException(e.getMessage(),e);
        }
    }
}
