package cn.donting.plugin.springboot.starter.extension;

import cn.donting.plugin.springboot.starter.application.PluginApplication;
import cn.donting.plugin.springboot.starter.entity.PluginInstallInfo;
import cn.donting.plugin.springboot.starter.exception.PluginException;
import cn.donting.plugin.springboot.starter.loader.PluginLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.ApplicationContext;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * 插件管理
 */
public abstract class AbsPluginManager  {
    private Logger log= LoggerFactory.getLogger(AbsPluginManager.class);
    /**
     * 插件安装目录
     */
    public static final String PLUGINS_PATH = System.getProperty("user.dir") + File.separator + "plugins";
    /**
     * 主程序释放的 插件 依赖（sdk等.....）
     */
    public static final String PLUGINS_LIB_PATH = System.getProperty("user.dir") + File.separator + "plugins" + File.separator + "pluginLib";

    /**
     * 运行中的插件
     */
    protected HashMap<String, PluginApplication> runPlugins = new HashMap<>();


    @Autowired
    ApplicationContext applicationContext;

    /**
     * 从文件安装插件
     * @param file
     */
    public abstract void install(File file) throws PluginException;

    /**
     * 卸载插件
     * @param pluginId 插件Id
     */
    public abstract void unInstall(String pluginId) throws PluginException;

    /**
     * 启动插件
     * @param pluginId pluginId
     */
    public abstract void startPlugin(String pluginId) throws PluginException;

    /**
     * 停止插件
     * @param pluginId pluginId
     * @return 退出码
     */
    public abstract int stopPlugin(String pluginId) throws PluginException;

    /**
     * 更新插件
     * @param file 文件
     */
    public abstract void updatePlugin(File file) throws PluginException;

    /**
     * 获取运行中的插件
     * @param pluginId  pluginId
     * @return PluginApplication
     */
    public PluginApplication getRunPlugin(String pluginId){
        return runPlugins.get(pluginId);
    }

    /**
     * 插件是否运行
     * @param pluginId
     * @return
     */
    public boolean isRunning(String pluginId){
        return runPlugins.containsKey(pluginId);
    }

    /**
     * 根据文件获取相应的插件加载器
     * @param file
     * @return
     * @throws PluginException 未找到加载的插件的加载器
     */
    public PluginLoader getPluginLoader(File file) throws PluginException {
        Map<String, PluginLoader> pluginLoaders = applicationContext.getBeansOfType(PluginLoader.class);
        for (Map.Entry<String, PluginLoader> stringPluginLoaderEntry : pluginLoaders.entrySet()) {
            PluginLoader pluginLoader = stringPluginLoaderEntry.getValue();
            pluginLoader.isLoader(file);
            return pluginLoader;
        }
        throw  new PluginException(file.getPath()+":找不到对应的插件加载器");
    }

    public void loadDevPlugin(PluginApplication pluginApplication) throws Exception {
        log.info("加载devPlugin name["+pluginApplication.getPluginInfo().getName()+"] id:[{}]",pluginApplication.getPluginInfo().getId());
        pluginApplication.run();
        runPlugins.put(pluginApplication.getPluginInfo().getId(),pluginApplication);
    }

}
