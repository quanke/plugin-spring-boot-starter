package cn.donting.plugin.springboot.starter.loader;


import cn.donting.plugin.springboot.starter.application.PluginApplication;
import cn.donting.plugin.springboot.starter.exception.PluginException;
import cn.donting.plugin.springboot.starter.extension.IPluginInfoLoader;
import cn.donting.springboot.plugin.top.domain.PluginInfo;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.File;

/**
 * 插件dev 加载器
 */
public abstract class PluginDevLoader {

    @Autowired
    private IPluginInfoLoader pluginInfoLoader;


    /**
     * 是否能够加载 文件为插件
     *
     * @param classLoader 类加载器
     * @return 是否
     */
    public abstract boolean isLoader(PluginURLClassLoader classLoader);

    /**
     * 从文件加载为插件
     *
     * @param classLoader 加载文件
     * @return PluginApplication
     * @throws Exception 加载异常
     */
    public abstract PluginApplication loader(PluginURLClassLoader classLoader, String mainClass) throws PluginException;


    /**
     * 获取 PluginInfo
     * @param mainClass 主类
     * @param classLoader 类加载器
     * @return PluginInfo
     * @throws PluginException
     */
    public final PluginInfo getPluginInfo(Class<?> mainClass,PluginURLClassLoader classLoader) throws PluginException {
        return pluginInfoLoader.get(mainClass, classLoader);
    }

}
