package cn.donting.springboot.gradle.plugin;

import org.gradle.api.Project;
import org.gradle.api.file.FileCollection;
import org.springframework.boot.gradle.plugin.ResolveMainClassName;

import java.io.File;
import java.lang.reflect.Method;
import java.util.Iterator;

/**
 * @author donting
 * 2021-07-18 下午2:30
 */
public class ClasspathUtil {
    public static String classpath(Project project,String split) throws Exception {
        try {

            StringBuilder stringBuilder = new StringBuilder();
            ResolveMainClassName bootRunMainClassName = (ResolveMainClassName) project.getTasks().getByName("bootRunMainClassName");
            FileCollection classpath = bootRunMainClassName.getClasspath();
            Iterator<File> iterator = classpath.iterator();
            while (iterator.hasNext()) {
                File file = iterator.next();
                stringBuilder.append(file.getPath()+split);
            }
            return stringBuilder.toString().substring(0,stringBuilder.length()-1);
        } catch (ClassCastException ex) {
            throw ex;
        } catch (Exception ex) {
            throw ex;
        }
    }

    public static String springbootMainClass(Project project) throws Exception {
        try {

            ResolveMainClassName bootRunMainClassName = (ResolveMainClassName) project.getTasks().getByName("bootRunMainClassName");
            Method resolveMainClassNameMethod = ResolveMainClassName.class.getDeclaredMethod("resolveMainClassName");
            resolveMainClassNameMethod.setAccessible(true);
            Object invoke = resolveMainClassNameMethod.invoke(bootRunMainClassName);
            return invoke.toString();
        } catch (ClassCastException | NoSuchMethodException ex) {
            System.err.println("检查是否使用  plugin: org.springframework.boot:2.5.0");
            throw ex;
        } catch (Exception ex) {
            throw ex;
        }

    }
}
