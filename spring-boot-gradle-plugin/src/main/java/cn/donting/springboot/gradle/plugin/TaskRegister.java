package cn.donting.springboot.gradle.plugin;

import cn.donting.springboot.gradle.plugin.extension.DevPlugin;
import cn.donting.springboot.gradle.plugin.extension.PluginExtension;
import cn.donting.springboot.gradle.plugin.task.PluginClasspathTask;
import cn.donting.springboot.gradle.plugin.task.PluginMainJar;
import cn.donting.springboot.gradle.plugin.task.PluginMainRun;
import cn.donting.springboot.gradle.plugin.task.PluginMainWar;
import org.gradle.api.Action;
import org.gradle.api.Plugin;
import org.gradle.api.Project;
import org.gradle.api.Task;
import org.gradle.api.file.FileCollection;
import org.gradle.api.internal.tasks.DefaultSourceSet;
import org.gradle.api.internal.tasks.DefaultSourceSetContainer;
import org.gradle.api.invocation.Gradle;
import org.gradle.api.provider.Property;
import org.gradle.api.tasks.SourceSet;
import org.gradle.api.tasks.TaskContainer;
import org.gradle.api.tasks.TaskProvider;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

public class TaskRegister implements Plugin<Project> {
    private Project project;

    @Override
    public void apply(Project project) {
        try {

            this.project = project;
            this.registerPluginMainRunTask();
            this.registerPluginMainJarTask();
            this.registerPluginMainWarTask();
            this.registerPluginExtension();
            isMainProject();
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    private void registerPluginMainRunTask() {
        PluginMainRun pluginRun = project.getTasks().create("pluginMainRun", PluginMainRun.class, (pluginMainRun) -> {
            try {
                pluginMainRun.setMain("cn.donting.springboot.plugin.run.Main");
                ArrayList<String> libs = pluginMainRun.commonLibTempPath();
                for (int i = 0; i < libs.size(); i++) {
                    if (libs.get(i).contains("plugin-sdk-spring-boot-starter")) {
                        libs.remove(i);
                        break;
                    }
                }
                pluginMainRun.setClasspath(project.files(libs.toArray()));
            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }


        });
        pluginRun.setGroup("plugin");
        pluginRun.dependsOn("build");
        project.getGradle().projectsEvaluated(new Action<Gradle>() {
            @Override
            public void execute(Gradle gradle) {
                PluginExtension pluginExtension = project.getExtensions().findByType(PluginExtension.class);
                List<String> pluginProjectPaths = pluginExtension.getPluginProjectPath().getOrElse(new ArrayList<String>());
                System.out.println("配置pluginProjectPaths：" + Arrays.toString(pluginProjectPaths.toArray()));
                for (String path : pluginProjectPaths) {
                    Project pluginProject = project.getRootProject().findProject(path);
                    if (pluginProject == null) {
                        throw new RuntimeException("为找到 project[" + path + "]");
                    }
                    //pluginRun 添加 插件的build编译
                    pluginRun.dependsOn(pluginProject.getTasks().getByName("build"));
                }
            }
        });
    }

    private void registerPluginMainJarTask() {
        PluginMainJar pluginMainJar = project.getTasks().create("pluginMainJar", PluginMainJar.class, (mainJar) -> {
        });
        pluginMainJar.setGroup("plugin");
        pluginMainJar.dependsOn("build");
    }

    private void registerPluginMainWarTask() {
        PluginMainWar pluginMainWar = project.getTasks().create("pluginMainWar", PluginMainWar.class, (mainWar) -> {
        });
        pluginMainWar.setGroup("plugin");
        pluginMainWar.dependsOn("bootWar");
    }

    private void registerPluginExtension() {
        project.getExtensions().create("pluginStater", PluginExtension.class, project);
    }

    private void isMainProject() {
//
//        DefaultSourceSetContainer sourceSets = (DefaultSourceSetContainer)project.getExtensions().getByName("sourceSets");
//        SourceSet main = sourceSets.getByName("main");
//        FileCollection runtimeClasspath = main.getRuntimeClasspath();
//        Set<File> files = runtimeClasspath.getFiles();
//        System.out.println(files.size());
    }

}
