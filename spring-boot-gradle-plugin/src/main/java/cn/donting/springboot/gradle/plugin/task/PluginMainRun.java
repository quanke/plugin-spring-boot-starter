package cn.donting.springboot.gradle.plugin.task;

import cn.donting.springboot.gradle.plugin.ClasspathUtil;
import cn.donting.springboot.gradle.plugin.extension.PluginExtension;
import org.apache.tools.ant.taskdefs.Java;
import org.gradle.api.Project;
import org.gradle.api.file.FileCollection;
import org.gradle.api.internal.file.collections.FileCollectionAdapter;
import org.gradle.api.internal.file.collections.FileTreeAdapter;
import org.gradle.api.internal.tasks.DefaultSourceSet;
import org.gradle.api.internal.tasks.DefaultSourceSetContainer;
import org.gradle.api.plugins.ExtensionContainer;
import org.gradle.api.provider.ListProperty;
import org.gradle.api.provider.Property;
import org.gradle.api.tasks.Internal;
import org.gradle.api.tasks.JavaExec;
import org.gradle.api.tasks.SourceSet;
import org.springframework.boot.gradle.tasks.run.BootRun;

import java.io.*;
import java.net.URL;
import java.util.*;

/**
 * main run
 */
public class PluginMainRun extends JavaExec {
    /**
     * dve插件 classpath ;分割
     * dve插件 mainClass
     * 多个 cn.donting.plugin.dev.classpath1=‘’ cn.donting.plugin.dev.mainClass1=‘’
     * cn.donting.plugin.dev.classpath2=‘’ cn.donting.plugin.dev.mainClass2=‘’
     */
    public static final String PREFIX_CLASSPATH = "--cn.donting.plugin.dev.classpath";
    public static final String PREFIX_MAIN_CLASS = "--cn.donting.plugin.dev.mainClass";
    public static final String DEV_PLUGIN_NUM = "--cn.donting.plugin.dev.pluginNum";

    public PluginMainRun() {
    }

    @Override
    public void exec() {
        Project project = getProject();
        ArrayList<String> args = new ArrayList<>();
        try {
            String classpath = ClasspathUtil.classpath(project, ";");
            args.add("--classpath=" + classpath);
            String mainClass = ClasspathUtil.springbootMainClass(project);
            args.add("--mainClass=" + mainClass);
            args.add("--spring.output.ansi.console-available=true");
            //开启spring boot 的彩色日志
            args.add("--spring.output.ansi.enabled=ALWAYS");
            //dev模式
            args.add("--cn.donting.plugin.dev=true");
            ArrayList<String> devPluginArgs = getDevPluginArgs();
            args.addAll(devPluginArgs);
            setArgs(args);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        try {
            super.exec();
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    /**
     * 获取dev 插件的 agrs 参数
     * @return
     */
    @Internal
    public ArrayList<String> getDevPluginArgs() {
        Project project = getProject();
        ArrayList<String> devArgs=new ArrayList<>();
        PluginExtension pe = project.getExtensions().findByType(PluginExtension.class);
        List<String> pluginMainClass = pe.getPluginMainClass().getOrElse(new ArrayList<>());
        List<String> pluginProjectPath = pe.getPluginProjectPath().getOrElse(new ArrayList<>());
        for (int i = 0; i < pluginProjectPath.size(); i++) {
            String projectPath = pluginProjectPath.get(i);
            Project pluginProject = project.getRootProject().findProject(projectPath);
            DefaultSourceSetContainer sourceSets = (DefaultSourceSetContainer)pluginProject.getExtensions().getByName("sourceSets");
            SourceSet main = sourceSets.getByName("main");
            FileCollection runtimeClasspath = main.getRuntimeClasspath();
            File[] files = runtimeClasspath.getFiles().toArray(new File[0]);
            String classpaths=arrayToString(files, ";");
            devArgs.add(PREFIX_CLASSPATH+i+"="+classpaths);
            devArgs.add(PREFIX_MAIN_CLASS+i+"="+pluginMainClass.get(i));
        }
        devArgs.add(DEV_PLUGIN_NUM+"="+pluginProjectPath.size());
        return devArgs;
    }


    public ArrayList<String> commonLibTempPath() throws IOException {
        ClassLoader classLoader = PluginMainRun.class.getClassLoader();
        URL inx = classLoader.getResource("commonLib/commonLib.idx");
        InputStream inputStream = inx.openStream();

        byte[] bytes = new byte[inputStream.available()];


        for (int i = 0; i < bytes.length; i++) {
            bytes[i] = (byte) inputStream.read();
        }
        inputStream.close();
        String inxText = new String(bytes);
        String[] libs = inxText.split("\n");
        File buildDir = getProject().getBuildDir();
        File commonLib = new File(buildDir + File.separator + "commonLib");
        if (!commonLib.exists()) {
            commonLib.mkdirs();
        }
        ArrayList<String> libsFilePath = new ArrayList<>();
        for (int i = 0; i < libs.length; i++) {
            URL lib = classLoader.getResource("commonLib/" + libs[i]);
            File file = new File(commonLib.getPath() + File.separator + libs[i]);
            if (!file.exists()) {
                file.createNewFile();
            }
            bytes = readInputStream(lib.openStream());
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            fileOutputStream.write(bytes);
            fileOutputStream.flush();
            fileOutputStream.close();
            libsFilePath.add(file.getPath());
        }
        return libsFilePath;
    }

    public byte[] readInputStream(InputStream inputStream) throws IOException {
        byte[] bytes = new byte[inputStream.available()];
        for (int i = 0; i < bytes.length; i++) {
            bytes[i] = (byte) inputStream.read();
        }
        inputStream.close();
        return bytes;
    }



    public String listToString(List list,String split){
        String str="";
        for (int i = 0; i < list.size(); i++) {
            if(i==list.size()-1){
                str+=list.get(i).toString();
            }else {
                str+=list.get(i).toString()+split;
            }
        }
        return str;
    }

    public String arrayToString(Object[] list,String split){
        String str="";
        for (int i = 0; i < list.length; i++) {
            if(i==list.length-1){
                str+=list[i].toString();
            }else {
                str+=list[i].toString()+split;
            }
        }
        return str;
    }
}
