package cn.donting.springboot.gradle.plugin.task;

import cn.donting.springboot.gradle.plugin.PluginJarWriter;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.ZipUtil;
import org.gradle.api.DefaultTask;
import org.gradle.api.Project;
import org.gradle.api.file.FileCollection;
import org.gradle.api.internal.TaskOutputsInternal;
import org.gradle.api.tasks.TaskAction;
import org.springframework.boot.gradle.tasks.bundling.BootJar;
import org.springframework.boot.loader.tools.JarWriter;
import org.springframework.boot.loader.tools.Library;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.List;
import java.util.jar.JarFile;

/**
 * main jar打包
 */
public class PluginMainJar extends DefaultTask {
    public byte[] getByte(InputStream inputStream) throws IOException {
        byte[] bytes = new byte[inputStream.available()];
        int by;
        int index = 0;
        while ((by = inputStream.read()) != -1) {
            bytes[index++] = (byte) by;
        }
        return bytes;
    }

    @TaskAction
    public void build() throws IOException {
        Project project = getProject();

        BootJar bootJar = (BootJar) project.getTasks().getByName("bootJar");
        TaskOutputsInternal outputs = bootJar.getOutputs();
        FileCollection files = outputs.getFiles();
        //booJar  sprinngboot 打包好的输出的文件
        File booJarOutFile = (File) files.getFiles().toArray()[0];

        //获取公共依包
        ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
        URL idx = contextClassLoader.getResource("commonLib/commonLib.idx");
        byte[] aByte = getByte(idx.openStream());
        String[] commonLibs = new String(aByte).split("\n");
//        HashSet<String> topLibSet = new HashSet<>();
//        for (String name : commonLibs) {
//            topLibSet.add(name);
//        }

        String prefix = FileUtil.getPrefix(booJarOutFile);

        //重新打包输出文件
        File targetFile = new File(booJarOutFile.getParentFile() + File.separator + prefix + "-plugin.jar");

        PluginJarWriter jarWriter = new PluginJarWriter(targetFile);
        //将 booJarOutFile class 写入 输出文件
        jarWriter.copyForBooJar(booJarOutFile);

        //解压 booJarOutFile 中的lib
        File unzip = ZipUtil.unzip(booJarOutFile);
        File libDir = new File(unzip + File.separator + "BOOT-INF" + File.separator + "lib");
        File[] libs = libDir.listFiles();
        //将 lib 写入 目标文件
        for (File lib : libs) {
            try {
                Library library = new Library(lib, null);
                //写入lib
                jarWriter.writeNestedLibrary("BOOT-INF/lib/", library);
            } catch (Exception e) {
                e.printStackTrace();
                jarWriter.close();
                return;
            }
        }

        //将公共依赖的 class 输入到 目标文件
        for (String name : commonLibs) {
            URL resource = contextClassLoader.getResource("commonLib/" + name);
            jarWriter.writeLoaderClassesNotJar(resource);
        }

        JarFile jarFile = new JarFile(booJarOutFile);
        jarWriter.writeManifest(jarFile.getManifest());
        jarWriter.close();
        //删除解压 的 临时目录
        FileUtil.del(unzip);
        System.out.println(targetFile);
    }
}
