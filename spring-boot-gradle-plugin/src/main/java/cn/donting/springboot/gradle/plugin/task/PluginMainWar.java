package cn.donting.springboot.gradle.plugin.task;

import cn.donting.springboot.gradle.plugin.PluginJarWriter;
import cn.hutool.core.io.FileUtil;
import org.gradle.api.DefaultTask;
import org.gradle.api.Project;
import org.gradle.api.file.FileCollection;
import org.gradle.api.internal.TaskOutputsInternal;
import org.gradle.api.tasks.TaskAction;
import org.springframework.boot.gradle.tasks.bundling.BootWar;
import org.springframework.boot.loader.tools.Library;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Set;
import java.util.jar.JarFile;

/**
 * main jar打包
 */
public class PluginMainWar extends DefaultTask {
    private static final String CLASSES_DIRECTORY = "WEB-INF/classes/";
//    private static final String ServletContainerInitializer = "META-INF/services/javax.servlet.ServletContainerInitializer";

    private static final String LIB_PROVIDED_DIRECTORY = "WEB-INF/lib-provided/";

    private static final String LIB_DIRECTORY = "WEB-INF/lib/";

    private static final String LAYERS_INDEX = "WEB-INF/layers.idx";

    public byte[] getByte(InputStream inputStream) throws IOException {
        byte[] bytes = new byte[inputStream.available()];
        int by;
        int index = 0;
        while ((by = inputStream.read()) != -1) {
            bytes[index++] = (byte) by;
        }
        return bytes;
    }

    @TaskAction
    public void build() throws IOException {
        Project project = getProject();

        BootWar bootWar = (BootWar) project.getTasks().getByName("bootWar");
        TaskOutputsInternal outputs = bootWar.getOutputs();
        FileCollection files = outputs.getFiles();
        //booJar  springboot 打包好的输出的文件
        File booWarOutFile = (File) files.getFiles().toArray()[0];
        String prefix = FileUtil.getPrefix(booWarOutFile);
        //重新打包输出文件
        File targetFile = new File(booWarOutFile.getParentFile() + File.separator + prefix + "-plugin.war");
        PluginJarWriter jarWriter = new PluginJarWriter(targetFile);
        //获取公共依包
        ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
        URL idx = contextClassLoader.getResource("commonLib/commonLib.idx");
        byte[] aByte = getByte(idx.openStream());
        String[] commonLibs = new String(aByte).split("\n");
        for (String name : commonLibs) {
            if (name.contains("servlet-api") || name.contains("websocket-api")) {
                continue;
            }
            URL resource = contextClassLoader.getResource("commonLib/" + name);
            File file = getFileForUrl(resource,name);
            Library library = new Library(file, null);
            jarWriter.writeNestedLibrary(LIB_DIRECTORY,library);
            file.delete();
        }
        URL url = booWarOutFile.toURI().toURL();
        jarWriter.writeLoaderClasses(url,(jarEntry)->{
            String name = jarEntry.getName();
            if (name.startsWith("WEB-INF")) {
                return name.replace("WEB-INF","BOOT-INF");
            }else{
                return null;
            }
        });
        JarFile jarFile = new JarFile(booWarOutFile);
        jarWriter.writeManifest(jarFile.getManifest());
        jarWriter.close();
    }


    private File getFileForUrl(URL resource,String name) {
        try {

            InputStream inputStream = resource.openStream();
            byte[] aByte = getByte(inputStream);
            String tmp = getProject().getBuildDir() + File.separator + "tmp" + File.separator+name;
            File file = new File(tmp);
            file.getParentFile().mkdirs();
            file.createNewFile();
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            fileOutputStream.write(aByte);
            fileOutputStream.close();
            return file;
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

}
