package cn.donting.springboot.gradle.plugin.extension;

import org.gradle.api.Project;
import org.gradle.api.internal.tasks.DefaultSourceSetContainer;
import org.gradle.api.provider.ListProperty;

import java.io.File;
import java.util.Set;

public class PluginExtension {
    private final Project project;

    /**
     * 插件的 project
     */
    private final ListProperty<String>  pluginProjectPath;
    private final ListProperty<String>  pluginMainClass;

    public PluginExtension(Project project) {
        this.project = project;
        this.pluginProjectPath = this.project.getObjects().listProperty(String.class);
        this.pluginMainClass = this.project.getObjects().listProperty(String.class);
    }

    public ListProperty<String> getPluginProjectPath() {
        return pluginProjectPath;
    }

    public ListProperty<String> getPluginMainClass() {
        return pluginMainClass;
    }
}
