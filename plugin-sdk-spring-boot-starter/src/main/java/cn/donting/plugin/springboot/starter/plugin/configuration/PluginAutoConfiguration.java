package cn.donting.plugin.springboot.starter.plugin.configuration;

import cn.donting.plugin.springboot.starter.plugin.web.PluginDispatcherServlet;
import cn.donting.springboot.plugin.top.PluginSpringWebApplicationContext;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.DispatcherServlet;

/**
 * @author donting
 * 2021-07-19 下午8:26
 */
@Configuration
public class PluginAutoConfiguration {

    @Bean
    public PluginDispatcherServlet pluginDispatcherServlet(DispatcherServlet dispatcherServlet, ApplicationContext applicationContext){
        return new PluginDispatcherServlet(dispatcherServlet,(PluginSpringWebApplicationContext) applicationContext);
    }
}
