package cn.donting.plugin.springboot.starter.plugin.web;

import cn.donting.springboot.plugin.top.PluginSpringWebApplicationContext;
import cn.donting.springboot.plugin.top.web.PluginWebDispatcher;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.*;
import java.io.IOException;
import java.util.Enumeration;

/**
 * PluginDispatcherServlet
 * @author donting
 */
@Slf4j
public class PluginDispatcherServlet {
    @Autowired
    private DispatcherServlet dispatcherServlet;

    public PluginDispatcherServlet(DispatcherServlet dispatcherServlet, PluginSpringWebApplicationContext pluginWebApplicationContext) {
        log.info("PluginDispatcherServlet.........");
        ServletContext servletContext = pluginWebApplicationContext.getServletContext();
        try {
            //初始化 dispatcherServlet
            dispatcherServlet.init(new ServletConfig() {
                @Override
                public String getServletName() {
                    return "getServletName";
                }

                @Override
                public ServletContext getServletContext() {
                    return servletContext;
                }

                @Override
                public String getInitParameter(String name) {
                    return "getInitParameter:"+name;
                }

                @Override
                public Enumeration<String> getInitParameterNames() {
                    return new Enumeration<String>() {
                        @Override
                        public boolean hasMoreElements() {
                            return false;
                        }

                        @Override
                        public String nextElement() {
                            return null;
                        }
                    };
                }
            });
            ServletConfig servletConfig = dispatcherServlet.getServletConfig();
        } catch (ServletException e) {
            e.printStackTrace();
        }
    }

    public void doService(ServletRequest httpRequest, ServletResponse httpResponse) throws ServletException, IOException {
        dispatcherServlet.service(httpRequest, httpResponse);
    }

//
//    private synchronized void initDispatcherServlet() throws ServletException {
//        if (!dispatcherServletIsInit) {
//            dispatcherServlet.init(PluginWebPostProcessor.pluginServletConfig);
//            dispatcherServletIsInit = true;
//        }
//    }
}