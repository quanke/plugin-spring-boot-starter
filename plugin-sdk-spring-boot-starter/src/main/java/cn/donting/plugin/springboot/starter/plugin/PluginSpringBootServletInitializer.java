package cn.donting.plugin.springboot.starter.plugin;

import cn.donting.plugin.springboot.starter.plugin.web.PluginAnnotationConfigServletWebServerApplicationContext;
import cn.donting.springboot.plugin.top.PluginSpringWebApplicationContext;
import org.springframework.boot.ApplicationContextFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.reactive.context.AnnotationConfigReactiveWebServerApplicationContext;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.web.context.WebApplicationContext;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import java.util.ArrayList;

/**
 * 插件 springboot web 的启动 类
 * 参照 war 在servlet 容器中的启动方式
 */
public class PluginSpringBootServletInitializer extends SpringBootServletInitializer {
    private Class<?> mainClass;
    private ServletContext servletContext;

    public PluginSpringBootServletInitializer(Class<?> mainClass) {
        this.mainClass = mainClass;
    }

    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {
        this.servletContext=servletContext;
        super.onStartup(servletContext);
    }

    public PluginSpringWebApplicationContext start(ServletContext servletContext){
        this.servletContext=servletContext;
        WebApplicationContext rootApplicationContext = this.createRootApplicationContext(servletContext);
        return (PluginSpringWebApplicationContext)rootApplicationContext;
    }

    @Override
    protected SpringApplicationBuilder createSpringApplicationBuilder() {
        PluginSpringApplicationBuilder springApplicationBuilder = new PluginSpringApplicationBuilder(servletContext);
        springApplicationBuilder.setMain(mainClass);
        ArrayList<Class<?>> arrayList=new ArrayList();
        arrayList.add(mainClass);
        springApplicationBuilder.application().addPrimarySources(arrayList);
        return springApplicationBuilder;
    }

    @Override
    protected WebApplicationContext createRootApplicationContext(ServletContext servletContext) {
        return super.createRootApplicationContext(servletContext);
    }

    private static class PluginSpringApplicationBuilder extends SpringApplicationBuilder {
        private ServletContext servletContext;

        public PluginSpringApplicationBuilder(ServletContext servletContext, Class<?>... sources) {
            super(sources);
            this.servletContext = servletContext;
            application().setApplicationContextFactory(new ApplicationContextFactory() {
                @Override
                public ConfigurableApplicationContext create(WebApplicationType webApplicationType) {
                    try {
                        switch (webApplicationType) {
                            case SERVLET:
                                return new PluginAnnotationConfigServletWebServerApplicationContext();
                            case REACTIVE:
                                return new AnnotationConfigReactiveWebServerApplicationContext();
                            default:
                                return new AnnotationConfigApplicationContext();
                        }
                    }
                    catch (Exception ex) {
                        throw new IllegalStateException("Unable create a default ApplicationContext instance, "
                                + "you may need a custom ApplicationContextFactory", ex);
                    }
                }
            });
        }

        @Override
        public SpringApplicationBuilder main(Class<?> mainApplicationClass) {
            return this;
        }

        public void setMain(Class<?> mainApplicationClass){
            super.application().setMainApplicationClass(mainApplicationClass);
        }

        @Override
        public SpringApplicationBuilder contextFactory(ApplicationContextFactory factory) {
            return this;
        }

        @Override
        protected SpringApplication createSpringApplication(Class<?>... sources) {
            return new PluginSpringApplication(servletContext,sources);
        }
    }
}
