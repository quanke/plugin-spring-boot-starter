package cn.donting.plugin.springboot.starter.plugin.web;

import cn.donting.springboot.plugin.top.PluginSpringWebApplicationContext;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.web.servlet.context.AnnotationConfigServletWebServerApplicationContext;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import java.io.IOException;

/**
 * @author donting
 * 2021-05-30 下午3:25
 */
public class PluginAnnotationConfigServletWebServerApplicationContext extends AnnotationConfigServletWebServerApplicationContext implements PluginSpringWebApplicationContext {
    public PluginAnnotationConfigServletWebServerApplicationContext() {
    }

    public PluginAnnotationConfigServletWebServerApplicationContext(DefaultListableBeanFactory beanFactory) {
        super(beanFactory);
    }



    public PluginAnnotationConfigServletWebServerApplicationContext(Class<?>... annotatedClasses) {
        super(annotatedClasses);
    }

    public PluginAnnotationConfigServletWebServerApplicationContext(String... basePackages) {
        super(basePackages);
    }

    @Override
    public void doService(ServletRequest httpRequest, ServletResponse httpResponse) throws ServletException, IOException {
        PluginDispatcherServlet bean = getBean(PluginDispatcherServlet.class);
        bean.doService(httpRequest, httpResponse);
    }

    @Override
    public String getProperties(String key) {
        return getEnvironment().getProperty(key);
    }

    @Override
    public int stopApplication() {
        return SpringApplication.exit(this);
    }
}
