### plugin-spring-boot-starter 插件式开发框架
    让你的springboot 动态加载springboot。
## 核心功能：
+ 主程序完整的springboot支持
+ 主程序支持war打包
+ 插件动态热拔插
+ 主程序，插件隔离，可用不同版本的Springboot
+ 扩展性强，预留扩展接口
+ 无需改造插件，只需要在resource下新增plugin.json文件，即可被主程序加载（当然你可以自定义 插件标识，例：文件名，插件主类上定义注解.....）
+ 插件独立性，插件不需要任何依赖，按照springboot打包为jar后即可被加载，也可独立运行。（不破环插件打包后的结构）
+ 插件几乎完整的spring-boot-starter支持
+ 提供gradle插件，用于开发需要。（maven插件暂未支持）
## 与springboot差异
 插件不支持jmx
## 插件支持
+  spring-boot-starter-web 
  差异：暂不支持serlet组件，如：Fliter，Servlet...。其他mvc，拦截器，静态资源均支持。
+ spring-boot-starter-web(暂未测试)
+ spring-boot-starter-data-jpa(暂未测试)
+ mybatis-spring-boot-starter(暂未测试)
+ mybatis-plus-boot-starter(暂未测试)

