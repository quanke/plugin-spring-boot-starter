package cn.donting.springboot.plugin.top;

import cn.donting.springboot.plugin.top.web.PluginWebDispatcher;

/**
 * 插件的 容器上下文
 */
public interface PluginWebApplicationContext extends PluginApplicationContext, PluginWebDispatcher {
}
