package cn.donting.springboot.plugin.top.domain;


import lombok.Data;

@Data
public class PluginInfo {
    private String name;
    private int numberVersion;
    private String version;
    private String id;
}
