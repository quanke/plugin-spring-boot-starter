package cn.donting.springboot.plugin.top.web;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import java.io.IOException;

/**
 * 插件 WebDispatcher 分发器
 * @author donting
 */
public interface PluginWebDispatcher {
    /**
     * doService
     * @param httpRequest
     * @param httpResponse
     */
    void doService(ServletRequest httpRequest, ServletResponse httpResponse) throws ServletException, IOException;
}
