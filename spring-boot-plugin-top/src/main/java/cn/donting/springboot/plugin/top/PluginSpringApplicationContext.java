package cn.donting.springboot.plugin.top;

import java.lang.annotation.Annotation;

/**
 * 插件的spring 容器上下文
 */
public interface PluginSpringApplicationContext extends PluginApplicationContext{
    String[] getBeanNamesForAnnotation(Class<? extends Annotation> annotationType);
}
