package cn.donting.springboot.plugin.top;

import java.lang.annotation.Annotation;

/**
 * 插件的 容器上下文
 */
public interface PluginApplicationContext {
    String getProperties(String key);
    Class<?> getType(String name);
    <T> T getBean(Class<T> requiredType) throws Exception;
    int stopApplication();
}
