package cn.donting.springboot.plugin.top;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;

/**
 * 插件 springboot web  Runner
 */
public interface PluginSpringWebRunner {
    PluginApplicationContext run(Class<?> mainClass, ServletContext servletContext, String... args) throws  Exception;
}
