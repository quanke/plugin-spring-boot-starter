package cn.donting.plugin.main.test;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.servlet.ServletWebServerFactoryAutoConfiguration;

@SpringBootApplication(exclude = ServletWebServerFactoryAutoConfiguration.class)
public class TestPluginApplication {
    public static void main(String[] args) {
        SpringApplication.run(TestPluginApplication.class,args);
    }
}
