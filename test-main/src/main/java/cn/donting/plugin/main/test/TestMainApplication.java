package cn.donting.plugin.main.test;


import cn.donting.plugin.springboot.starter.condition.OnPluginCondition;
import cn.donting.plugin.springboot.starter.loader.boot.BootJarPluginLoader;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

import javax.servlet.ServletContainerInitializer;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.ServiceLoader;

/**
 * @author donting
 */
@SpringBootApplication
public class TestMainApplication extends SpringBootServletInitializer {
    public static void main(String[] args) {
        ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
        ServiceLoader<ServletContainerInitializer> load = ServiceLoader.load(ServletContainerInitializer.class);
        Iterator<ServletContainerInitializer> iterator = load.iterator();
        ArrayList arrayList=new ArrayList();
        while (iterator.hasNext()) {
            arrayList.add(iterator.next());
        }
        SpringApplication.run(TestMainApplication.class,args);
    }

    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {
        ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
        ServiceLoader<ServletContainerInitializer> uploadCDN = ServiceLoader.load(ServletContainerInitializer.class);
       ArrayList arrayList=new ArrayList();
        Iterator<ServletContainerInitializer> iterator = uploadCDN.iterator();
        while (iterator.hasNext()) {
            ServletContainerInitializer next = iterator.next();
            ClassLoader classLoader = next.getClass().getClassLoader();
            arrayList.add(next);
        }

        super.onStartup(servletContext);
    }
}
