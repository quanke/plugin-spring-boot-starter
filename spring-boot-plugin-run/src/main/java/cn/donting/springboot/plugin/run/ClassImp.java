package cn.donting.springboot.plugin.run;

import org.apache.bcel.classfile.ClassParser;
import org.apache.bcel.classfile.JavaClass;
import org.apache.bcel.util.ClassLoaderRepository;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

public class ClassImp {

    public static HashSet<Class<?>> isImp(URLClassLoader urlClassLoader, Class<?>[] classes) throws URISyntaxException, IOException {
        HashSet<Class<?>> result = new HashSet<>();
        HashSet<String> className = new HashSet<>();
        for (Class<?> c : classes) {
            className.add(c.getName());
        }
        URL[] urLs = urlClassLoader.getURLs();
        for (URL urL : urLs) {
            File file = new File(urL.toURI().getPath());
            isImp(file, urlClassLoader, className, result);
        }
        return result;
    }

    private static void isImp(File file, URLClassLoader classLoader, HashSet<String> className, HashSet<Class<?>> result) throws IOException {
        if (file.isDirectory()) {
            File[] files = file.listFiles();
            for (File file1 : files) {
                isImp(file1, classLoader, className, result);
            }
        }
        if (file.isFile() && file.getName().endsWith(".class")) {
            ClassParser classParser = new ClassParser(file.getPath());
            check(file, classLoader, className, result, classParser);

        }
        if (file.isFile() && file.getName().endsWith(".jar")) {
            JarFile jarFile = new JarFile(file);
            Enumeration<JarEntry> entries = jarFile.entries();
            while (entries.hasMoreElements()) {
                JarEntry jarEntry = entries.nextElement();
                if (jarEntry.getName().endsWith(".class")) {
                    ClassParser classParser = new ClassParser(file.getPath(), jarEntry.getName());
                    check(file, classLoader, className, result, classParser);

                }
            }


        }


    }

    private static void check(File file, URLClassLoader classLoader, HashSet<String> className, HashSet<Class<?>> result, ClassParser classParser) throws IOException {
        JavaClass parse = classParser.parse();
        parse.setRepository(new ClassLoaderRepository(classLoader));
        JavaClass[] allInterfaces = new JavaClass[0];
        try {
            allInterfaces = parse.getAllInterfaces();
            for (JavaClass allInterface : allInterfaces) {
                if (className.contains(allInterface.getClassName())) {
                    Class<?> aClass = classLoader.loadClass(parse.getClassName());
                    result.add(aClass);
                }
            }
        } catch (ClassNotFoundException e) {
        }
    }
}
