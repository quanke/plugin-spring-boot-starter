package cn.donting.springboot.plugin.run;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Args {
    private final String[] args;

    private String mainClass;
    private String[] pluginMainArgs;
    private List<String> classpath=new ArrayList<>();

    public Args(String[] args) {
        this.args = args;
        init();
    }

    private void init() {
        ArrayList<String> pluginMainArgs = new ArrayList<>();
        for (String arg : args) {
            if (arg.toLowerCase().startsWith("--mainclass")) {
                try {
                    mainClass = arg.split("=")[1];
                } catch (Exception ex) {
                    ex.printStackTrace();
                    throw new RuntimeException(arg + " 参数解析异常[--mainclass]");
                }
                continue;
            }
            if (arg.toLowerCase().startsWith("--classpath")) {
                try {
                    String classpath = arg.split("=")[1];
                    String[] split = classpath.split(";");
                    this.classpath = Arrays.asList(split);
                } catch (Exception ex) {
                    ex.printStackTrace();
                    throw new RuntimeException(arg + " 参数解析异常[--classpath]");
                }
                continue;
            }
            pluginMainArgs.add(arg);
        }
        if (this.mainClass == null) {
           throw  new RuntimeException("缺失 mainClass");
        }
        this.pluginMainArgs=pluginMainArgs.toArray(new String[pluginMainArgs.size()]);
    }

    public List<String> getClasspath() {
        return classpath;
    }

    public String getMainClass() {
        return mainClass;
    }

    public String[] getPluginMainArgs() {
        return pluginMainArgs;
    }
}
