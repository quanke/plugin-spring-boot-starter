package cn.donting.springboot.plugin.run;

import java.net.URL;
import java.net.URLClassLoader;

public class PluginMainClassLoader extends URLClassLoader {
    public PluginMainClassLoader(URL[] urls) {
        super(urls);
    }

    public PluginMainClassLoader(URL[] urls, ClassLoader parent) {
        super(urls, parent);
    }

    @Override
    public Class<?> loadClass(String name) throws ClassNotFoundException {
        return super.loadClass(name);
    }
}
